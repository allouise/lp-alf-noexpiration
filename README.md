## ALF LearnPress No Expiration
---

### Description

Custom Add-on for Wordpres Premium Plugin LearnPress - It prevents Courses & Quizzes from Expiring.
Administrators has simple & easy interface to Modify Course & Quizzes.

This will allow Students to retake the Courses & Quizzes without Expiration.
#### For Customizations on Wordpress Contact me at elixirlouise@gmail.com