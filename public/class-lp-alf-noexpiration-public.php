<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://bitbucket.org/allouise/alf-learnpress-no-expiration
 * @since      1.0.0
 *
 * @package    LP_ALF_NOEXP
 * @subpackage LP_ALF_NOEXP/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    LP_ALF_NOEXP
 * @subpackage LP_ALF_NOEXP/public
 * @author     Allyson Flores <elixirlouise@gmail.com>
 */
class LP_ALF_NOEXP_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $lp_alf_noexpiration    The ID of this plugin.
	 */
	private $lp_alf_noexpiration, $wpdb;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $lp_alf_noexpiration       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $lp_alf_noexpiration, $version ) {
		global $wpdb;
		$this->lp_alf_noexpiration = $lp_alf_noexpiration;
		$this->version = $version;
		$this->wpdb = $wpdb;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in LP_ALF_NOEXP_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The LP_ALF_NOEXP_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->lp_alf_noexpiration, plugin_dir_url( __FILE__ ) . 'css/lp-alf-noexpiration-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in LP_ALF_NOEXP_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The LP_ALF_NOEXP_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->lp_alf_noexpiration, plugin_dir_url( __FILE__ ) . 'js/lp-alf-noexpiration-public.js', array( 'jquery' ), $this->version, false );

	}


	/**
	 * Alter If LP
	 *
	 * @since    1.0.0
	 */
	public function learn_press_modification() {
		global $post;
		$allowed_post = array( 'lp_course', 'lp_lesson', 'lp_quiz' );
		$user = get_current_user_id();
		$posttype = get_post_type($post->ID);

		if( isset( $post->ID ) && in_array($posttype, $allowed_post) && $user > 0 ){

			$return = $this->wpdb->get_row( "SELECT * FROM {$this->wpdb->prefix}learnpress_user_items WHERE ".( $posttype == 'lp_course'? " status = 'finished' AND " : "" )." user_id = $user AND item_id = {$post->ID}" );
			$post_id = null;

			if ( null !== $return ) {

				switch ($return->item_type) {
					case 'lp_course':
						$post_id = $return->item_id;
						break;
					case 'lp_quiz':
					case 'lp_lesson':
						$post_id = $return->ref_id;
						$return = $this->wpdb->get_row( "SELECT * FROM {$this->wpdb->prefix}learnpress_user_items WHERE status = 'finished' AND user_id = $user AND item_id = $post_id" );
					default:
						
						break;
				}

				$no_exp = get_post_meta( $post_id, 'alf_course_no_exp', true );

				if( isset($post_id) && isset($no_exp) && $no_exp == 'enabled' && isset($return->status) && $return->status == 'finished' ){
					$this->wpdb->update( 
						"{$this->wpdb->prefix}learnpress_user_items", 
						array( 
							'end_time' => '0000-00-00 00:00:00',
							'end_time_gmt' => '0000-00-00 00:00:00',
							'status' => 'enrolled'
						), 
						array( 
							'user_id' => $user, 
							'item_id' => $post_id,
							'status' => 'finished',
							'item_type' => 'lp_course'
						), 
						array( 
							'%s',
							'%s',
							'%s'
						), 
						array(
							'%d',
							'%d',
							'%s',
							'%s'
						) 
					);
				}

			}
		}
	}

	/*
	 * Redo Button
	 */
	public function redo_button(){
		echo "<span class='no_exp'></span>";
	}
}
