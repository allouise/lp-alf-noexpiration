=== ALF LearnPress No Expiration ===
Donate link: https://paypal.me/allouise
Tags: learnpress, quiz, expire, course, no-expire, no-expiration, no, expiration
Requires at least: 5.0
Tested up to: 5.3.2
Stable tag: 5.3.2
Requires PHP: 5.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

LearnPress Customization for No Expiring Courses & Quizzes

== Description ==

LearnPress Customization for No Expiring Courses & Quizzes
Report/Post Bugs here:https://bitbucket.org/allouise/alf-learnpress-no-expiration/

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress

== Screenshots ==

1. ALF LearnPress No Expiration

== Changelog ==

= 0.1.0 (Feb 27, 2020) =

- First release.