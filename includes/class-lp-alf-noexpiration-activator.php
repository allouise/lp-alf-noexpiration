<?php

/**
 * Fired during plugin activation
 *
 * @link       https://bitbucket.org/allouise/alf-learnpress-no-expiration
 * @since      1.0.0
 *
 * @package    LP_ALF_NOEXP
 * @subpackage LP_ALF_NOEXP/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    LP_ALF_NOEXP
 * @subpackage LP_ALF_NOEXP/includes
 * @author     Allyson Flores <elixirlouise@gmail.com>
 */
class LP_ALF_NOEXP_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		
	}

}
