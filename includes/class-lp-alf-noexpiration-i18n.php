<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://bitbucket.org/allouise/alf-learnpress-no-expiration
 * @since      1.0.0
 *
 * @package    LP_ALF_NOEXP
 * @subpackage LP_ALF_NOEXP/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    LP_ALF_NOEXP
 * @subpackage LP_ALF_NOEXP/includes
 * @author     Allyson Flores <elixirlouise@gmail.com>
 */
class LP_ALF_NOEXP_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'lp-alf-noexpiration',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
