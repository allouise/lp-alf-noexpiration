<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://bitbucket.org/allouise/alf-learnpress-no-expiration
 * @since      1.0.0
 *
 * @package    LP_ALF_NOEXP
 * @subpackage LP_ALF_NOEXP/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    LP_ALF_NOEXP
 * @subpackage LP_ALF_NOEXP/includes
 * @author     Allyson Flores <elixirlouise@gmail.com>
 */
class LP_ALF_NOEXP {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      LP_ALF_NOEXP_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $lp_alf_noexpiration    The string used to uniquely identify this plugin.
	 */
	protected $lp_alf_noexpiration;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'LP_ALF_NOEXP_VERSION' ) ) {
			$this->version = LP_ALF_NOEXP_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->lp_alf_noexpiration = 'lp-alf-noexpiration';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - LP_ALF_NOEXP_Loader. Orchestrates the hooks of the plugin.
	 * - LP_ALF_NOEXP_i18n. Defines internationalization functionality.
	 * - LP_ALF_NOEXP_Admin. Defines all hooks for the admin area.
	 * - LP_ALF_NOEXP_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-lp-alf-noexpiration-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-lp-alf-noexpiration-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-lp-alf-noexpiration-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-lp-alf-noexpiration-public.php';

		$this->loader = new LP_ALF_NOEXP_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the LP_ALF_NOEXP_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new LP_ALF_NOEXP_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new LP_ALF_NOEXP_Admin( $this->get_lp_alf_noexpiration(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'add_meta_boxes', $plugin_admin, 'register_metaboxes' );
		$this->loader->add_action( 'save_post', $plugin_admin, 'course_metabox_save' );
		$this->loader->add_action( 'save_post', $plugin_admin, 'quiz_metabox_save' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new LP_ALF_NOEXP_Public( $this->get_lp_alf_noexpiration(), $this->get_version() );

		/*$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );*/
		/*$this->loader->add_action( 'learn_press_before_main_content', $plugin_public, 'learn_press_before_main_content', 20 );
		$this->loader->add_action( 'learn_press_before_content_in_single_quiz', $plugin_public, 'learn_press_before_content_in_single_quiz', 20 );
		$this->loader->add_action( 'learn_press_course_content_lesson', $plugin_public, 'learn_press_course_content_lesson', 20 );*/
		$this->loader->add_action( 'wp', $plugin_public, 'learn_press_modification' );
		$this->loader->add_action( 'learn-press/begin-quiz-redo-button', $plugin_public, 'redo_button' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_lp_alf_noexpiration() {
		return $this->lp_alf_noexpiration;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    LP_ALF_NOEXP_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
