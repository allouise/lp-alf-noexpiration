<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://bitbucket.org/allouise/alf-learnpress-no-expiration
 * @since      1.0.0
 *
 * @package    LP_ALF_NOEXP
 * @subpackage LP_ALF_NOEXP/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    LP_ALF_NOEXP
 * @subpackage LP_ALF_NOEXP/admin
 * @author     Allyson Flores <elixirlouise@gmail.com>
 */
class LP_ALF_NOEXP_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $lp_alf_noexpiration    The ID of this plugin.
	 */
	private $lp_alf_noexpiration;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $lp_alf_noexpiration       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $lp_alf_noexpiration, $version ) {

		$this->lp_alf_noexpiration = $lp_alf_noexpiration;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in LP_ALF_NOEXP_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The LP_ALF_NOEXP_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->lp_alf_noexpiration, plugin_dir_url( __FILE__ ) . 'css/lp-alf-noexpiration-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in LP_ALF_NOEXP_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The LP_ALF_NOEXP_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->lp_alf_noexpiration, plugin_dir_url( __FILE__ ) . 'js/lp-alf-noexpiration-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Register Metaboxes
	 *
	 * @since    1.0.0
	 */
	public function register_metaboxes() {
		add_meta_box( 'lp-alf-noexpiration-course-metabox', __( 'LearnPress Course Custom', $this->lp_alf_noexpiration ), array( $this, 'course_metabox_callback' ), 'lp_course', 'advanced', 'high' );
		add_meta_box( 'lp-alf-noexpiration-quiz-metabox', __( 'LearnPress Quiz Custom', $this->lp_alf_noexpiration ), array( $this, 'quiz_metabox_callback' ), 'lp_quiz', 'advanced', 'high' );
	}

	/**
	 * Course Meta box display callback.
	 *
	 * @param WP_Post $post Current post object.
	 */
	public function course_metabox_callback( $post ) {
	    require_once plugin_dir_path( __FILE__ ). 'partials/lp-alf-noexpiration-admin-course-metabox.php';
	}

	/**
	 * Quiz Meta box display callback.
	 *
	 * @param WP_Post $post Current post object.
	 */
	public function quiz_metabox_callback( $post ) {
	    require_once plugin_dir_path( __FILE__ ). 'partials/lp-alf-noexpiration-admin-quiz-metabox.php';
	}
	 
	/**
	 * Save Course meta box content.
	 *
	 * @param int $post_id Post ID
	 */
	public function course_metabox_save( $post_id ) {
	    $nonce_name   = isset( $_POST['lp_alf_noexp_course_meta'] ) ? $_POST['lp_alf_noexp_course_meta'] : '';
        $nonce_action = 'lp_alf_noexp_course_meta_action';
        $field = 'alf_course_no_exp';

        // Check if nonce is valid.
        if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {
            return;
        }
 
        // Check if user has permissions to save data.
        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
 
        // Check if not an autosave.
        if ( wp_is_post_autosave( $post_id ) ) {
            return;
        }
 
        // Check if not a revision.
        if ( wp_is_post_revision( $post_id ) ) {
            return;
        }

        if ( array_key_exists( $field, $_POST ) ) {
        	$value = ( $_POST[$field] == 'enabled' )? 'enabled' : 'disabled';
            update_post_meta( $post_id, $field, sanitize_text_field( $value ) );

            if( $value == 'enabled' ){
            	update_post_meta( $post_id, '_lp_retake_count', '9999' );
            	update_post_meta( $post_id, '_lp_duration', '0 minute' );
            }
        }
	}
	
	/**
	 * Save Quiz meta box content.
	 *
	 * @param int $post_id Post ID
	 */
	public function quiz_metabox_save( $post_id ) {
	    $nonce_name   = isset( $_POST['lp_alf_noexp_quiz_meta'] ) ? $_POST['lp_alf_noexp_quiz_meta'] : '';
        $nonce_action = 'lp_alf_noexp_quiz_meta_action';
        $field = 'alf_quiz_no_exp';

        // Check if nonce is valid.
        if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {
            return;
        }
 
        // Check if user has permissions to save data.
        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
 
        // Check if not an autosave.
        if ( wp_is_post_autosave( $post_id ) ) {
            return;
        }
 
        // Check if not a revision.
        if ( wp_is_post_revision( $post_id ) ) {
            return;
        }

        if ( array_key_exists( $field, $_POST ) ) {
        	$value = ( $_POST[$field] == 'enabled' )? 'enabled' : 'disabled';
            update_post_meta( $post_id, $field, sanitize_text_field( $value ) );

            if( $value == 'enabled' ){
            	update_post_meta( $post_id, '_lp_retake_count', '9999' );
            	update_post_meta( $post_id, '_lp_duration', ' minute' );
            }
        }
	}
}
