<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://bitbucket.org/allouise/alf-learnpress-no-expiration
 * @since      1.0.0
 *
 * @package    LP_ALF_NOEXP
 * @subpackage LP_ALF_NOEXP/admin/partials
 */
?>

<div id="lp_alf_noexp_course_meta_box">
	<?php 
		wp_nonce_field( 'lp_alf_noexp_course_meta_action', 'lp_alf_noexp_course_meta' ); 
		$alf_course_no_exp = get_post_meta($post->ID, 'alf_course_no_exp', true);
		/*var_dump($alf_course_no_exp);*/
	?>
	<p class="meta-options hcf_field">
        <label style="color: #da0000; font-weight: bold;" for="alf_course_no_exp">Make Course Doesn't Expire</label>
        <br/>
        <select id="alf_course_no_exp" name="alf_course_no_exp" style="width: 100%;">
        	<option value="disabled" <?php echo ( isset($alf_course_no_exp) && $alf_course_no_exp == 'disabled' )? "selected" : ""; ?>>No</option>
        	<option value="enabled" <?php echo ( isset($alf_course_no_exp) && $alf_course_no_exp == 'enabled' )? "selected" : ""; ?>>Yes</option>
        </select>
    </p>
</div id="lp_alf_noexp_course_meta_box">
<script type="text/javascript">
	$(window).on('load', function() {
		_check_alf_noexp_course_checked($('#alf_course_no_exp'));
		$('#alf_course_no_exp').on('click',function(){
			_check_alf_noexp_course_checked($(this));
		});

		function _check_alf_noexp_course_checked( _this ){
			/*console.log(_this.val());*/
			if( _this.val() == 'enabled' ){
				$('#_lp_duration_select').val('week');
				/*console.log($('#_lp_duration_select').val());*/
				$('#_lp_duration').parents('.rwmb-field').css({'opacity':'0','width':'0','height':'0','oveflow':'hidden','position':'absolute','z-index':'-1'});
				$('#_lp_retake_count').parents('.rwmb-field').css({'opacity':'0','width':'0','height':'0','oveflow':'hidden','position':'absolute','z-index':'-1'});
			}else{
				$('#_lp_duration').parents('.rwmb-field').removeAttr('style');
				$('#_lp_retake_count').parents('.rwmb-field').removeAttr('style');
			}
		}
	});
</script>
