<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://bitbucket.org/allouise/alf-learnpress-no-expiration
 * @since             1.0.0
 * @package           LP_ALF_NOEXP
 *
 * @wordpress-plugin
 * Plugin Name:       ALF LearnPress No Expiration
 * Plugin URI:        https://bitbucket.org/allouise/alf-learnpress-no-expiration/
 * Description:       LearnPress Customization for No Expiring Courses & Quizzes
 * Version:           1.0.0
 * Author:            Allyson Flores
 * Author URI:        http://allysonflores.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       lp-alf-noexpiration
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'LP_ALF_NOEXP_VERSION', '1.0.0' );
define( 'LP_ALF_NOEXP_LEARNPRESS_VERSION', ( defined('LEARNPRESS_VERSION') )? LEARNPRESS_VERSION : false );
define( 'LP_ALF_NOEXP_LP_PLUGIN_PATH', ( defined('LP_PLUGIN_PATH') )? LP_PLUGIN_PATH : false );

/*
 * Check for LearnPress Dependencies
 */
function dependency_lp_alf_noexpiration( $cons = false ){
	if( !defined('LEARNPRESS_VERSION') || !defined('LP_PLUGIN_PATH') ){
		require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		deactivate_plugins( plugin_basename( __FILE__ ) );

		if( $cons === true ){
			wp_die( __('<p><strong>ALF LearnPress No Expiration:</strong> Please make sure <strong>LearnPress</strong> is installed & activated<p>').'<a href="'.get_admin_url().'plugins.php">'.__('Go Back').'</a>' );
		}else{
			echo '<div class="error" style="-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif">'.__('<p><strong>ALF LearnPress No Expiration:</strong> Please make sure <strong>LearnPress</strong> is installed & activated<p>').'</div>';
		}
	}
}
add_action( 'plugins_loaded', 'dependency_lp_alf_noexpiration');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-lp-alf-noexpiration-activator.php
 */
function activate_lp_alf_noexpiration() {
	dependency_lp_alf_noexpiration(true);
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-lp-alf-noexpiration-activator.php';
	LP_ALF_NOEXP_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-lp-alf-noexpiration-deactivator.php
 */
function deactivate_lp_alf_noexpiration() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-lp-alf-noexpiration-deactivator.php';
	LP_ALF_NOEXP_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_lp_alf_noexpiration' );
register_deactivation_hook( __FILE__, 'deactivate_lp_alf_noexpiration' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-lp-alf-noexpiration.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_lp_alf_noexpiration() {

	$plugin = new LP_ALF_NOEXP();
	$plugin->run();

}
run_lp_alf_noexpiration();
